pub struct Solution;

impl Solution {
    pub fn is_palindrome(s: String) -> bool {
        let letters = s
            .chars()
            .flat_map(|c| c.to_lowercase())
            .filter(|c| c.is_alphanumeric())
            .collect::<String>();

        letters == letters.chars().rev().collect::<String>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(expected: bool, s: String) {
        let actual = Solution::is_palindrome(s);
        assert_eq!(actual, expected);
    }

    #[test]
    fn case_1() {
        run_case(true, String::from("A man, a plan, a canal: Panama"));
    }

    #[test]
    fn case_2() {
        run_case(false, String::from("race a car"));
    }

    #[test]
    fn case_3() {
        run_case(true, String::from(" "));
    }

    #[test]
    fn case_4() {
        run_case(false, String::from("0P"));
    }
}
