// https://leetcode.com/problems/evaluate-reverse-polish-notation/
pub struct Solution;

use std::convert::TryFrom;
use std::ops::{Add, Div, Mul, Sub};

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Operator {
    Add,
    Subtract,
    Multiply,
    Divide,
}

impl Operator {
    pub fn apply<N>(&self, left: N, right: N) -> N
    where
        N: Add<Output = N> + Sub<Output = N> + Mul<Output = N> + Div<Output = N>,
    {
        match self {
            Operator::Add => left + right,
            Operator::Subtract => left - right,
            Operator::Multiply => left * right,
            Operator::Divide => left / right,
        }
    }
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Token {
    Operator(Operator),
    Number(i32),
}

impl TryFrom<&str> for Token {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.trim() {
            "+" => Ok(Token::Operator(Operator::Add)),
            "-" => Ok(Token::Operator(Operator::Subtract)),
            "*" => Ok(Token::Operator(Operator::Multiply)),
            "/" => Ok(Token::Operator(Operator::Divide)),
            v => {
                if let Ok(num) = v.parse() {
                    Ok(Token::Number(num))
                } else {
                    Err(format!("Invalid token: {v}"))
                }
            }
        }
    }
}

impl Solution {
    pub fn eval_rpn(raw_tokens: Vec<String>) -> i32 {
        let tokens = raw_tokens
            .iter()
            .filter_map(|raw_token| Token::try_from(raw_token.as_str()).ok());

        let mut stack: Vec<i32> = vec![];

        for token in tokens {
            match token {
                Token::Number(num) => stack.push(num),
                Token::Operator(op) => {
                    let right = stack.pop();
                    let left = stack.pop();

                    if let (Some(left), Some(right)) = (left, right) {
                        stack.push(op.apply(left, right));
                    }
                }
            }
        }

        *stack.last().unwrap_or(&0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(expected: i32, tokens: Vec<String>) {
        let actual = Solution::eval_rpn(tokens);
        assert_eq!(actual, expected);
    }

    #[test]
    fn case_1() {
        let tokens = vec![
            String::from("2"),
            String::from("1"),
            String::from("+"),
            String::from("3"),
            String::from("*"),
        ];

        run_case(9, tokens);
    }

    #[test]
    fn case_2() {
        let tokens = vec![
            String::from("4"),
            String::from("13"),
            String::from("5"),
            String::from("/"),
            String::from("+"),
        ];

        run_case(6, tokens);
    }

    #[test]
    fn case_3() {
        let tokens = vec![
            String::from("10"),
            String::from("6"),
            String::from("9"),
            String::from("3"),
            String::from("+"),
            String::from("-11"),
            String::from("*"),
            String::from("/"),
            String::from("*"),
            String::from("17"),
            String::from("+"),
            String::from("5"),
            String::from("+"),
        ];

        run_case(22, tokens);
    }
}
