// https://leetcode.com/problems/valid-parentheses/
pub struct Solution;

fn get_opening_char(ch: char) -> Option<char> {
    match ch {
        ']' => Some('['),
        '}' => Some('{'),
        ')' => Some('('),
        _ => None,
    }
}

impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut stack: Vec<char> = vec![];

        for ch in s.chars() {
            match get_opening_char(ch) {
                None => stack.push(ch),
                Some(opening_ch) => {
                    let prev = stack.pop();

                    if prev != Some(opening_ch) {
                        return false;
                    }
                }
            }
        }

        stack.is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(expected: bool, s: String) {
        let actual = Solution::is_valid(s);
        assert_eq!(actual, expected);
    }

    #[test]
    fn case_1() {
        run_case(true, String::from("()"))
    }

    #[test]
    fn case_2() {
        run_case(true, String::from("()[]{}"))
    }

    #[test]
    fn case_3() {
        run_case(false, String::from("(]"))
    }

    #[test]
    fn case_4() {
        run_case(true, String::from("{[]}"))
    }

    #[test]
    fn case_5() {
        run_case(false, String::from("["))
    }
}
