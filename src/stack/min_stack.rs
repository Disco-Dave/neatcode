// https://leetcode.com/problems/valid-parentheses/
pub struct MinStack {
    stack: Vec<i32>,
    min: Vec<i32>,
}

impl MinStack {
    pub fn new() -> Self {
        Self {
            stack: vec![],
            min: vec![],
        }
    }

    pub fn push(&mut self, val: i32) {
        self.stack.push(val);

        match self.min.last() {
            Some(&min) if val <= min => self.min.push(val),
            None => self.min.push(val),
            _ => (),
        };
    }

    pub fn pop(&mut self) {
        let val_option = self.stack.pop();

        match (val_option, self.min.last()) {
            (Some(val), Some(&min)) if val == min => {
                self.min.pop();
            }
            _ => (),
        }
    }

    pub fn top(&self) -> i32 {
        *self.stack.last().unwrap_or(&0)
    }

    pub fn get_min(&self) -> i32 {
        *self.min.last().unwrap_or(&0)
    }
}

impl Default for MinStack {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    enum Instruction {
        Push(i32),
        Pop,
        Top,
        GetMin,
    }

    fn run_case(expected: &[i32], instructions: &[Instruction]) {
        let mut min_stack = MinStack::new();
        let mut actual: Vec<i32> = vec![];

        for instruction in instructions {
            match instruction {
                Instruction::Push(val) => min_stack.push(*val),
                Instruction::Pop => min_stack.pop(),
                Instruction::Top => actual.push(min_stack.top()),
                Instruction::GetMin => actual.push(min_stack.get_min()),
            }
        }

        assert_eq!(actual, expected)
    }

    #[test]
    fn case_1() {
        let expected = [-3, 0, -2];

        let instructions = [
            Instruction::Push(-2),
            Instruction::Push(0),
            Instruction::Push(-3),
            Instruction::GetMin,
            Instruction::Pop,
            Instruction::Top,
            Instruction::GetMin,
        ];

        run_case(&expected, &instructions);
    }
}
