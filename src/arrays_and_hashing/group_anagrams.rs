/// https://leetcode.com/problems/group-anagrams/
pub struct Solution;

use std::collections::HashMap;

impl Solution {
    pub fn group_anagrams(strs: Vec<String>) -> Vec<Vec<String>> {
        let mut anagrams: HashMap<Vec<char>, Vec<String>> = HashMap::new();

        for word in strs {
            let mut char_vec: Vec<char> = word.chars().collect();

            char_vec.sort();

            anagrams
                .entry(char_vec)
                .and_modify(|v| v.push(word.clone()))
                .or_insert_with(|| vec![word.clone()]);
        }

        anagrams.into_values().collect()
    }
}
