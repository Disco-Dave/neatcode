/// https://leetcode.com/problems/valid-sudoku
pub struct Solution;

use std::collections::BTreeSet;
use std::iter::FromIterator;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
enum Digit {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
}

impl Digit {
    fn parse(ch: char) -> Option<Self> {
        match ch.to_lowercase().collect::<String>().as_ref() {
            "1" => Some(Digit::One),
            "2" => Some(Digit::Two),
            "3" => Some(Digit::Three),
            "4" => Some(Digit::Four),
            "5" => Some(Digit::Five),
            "6" => Some(Digit::Six),
            "7" => Some(Digit::Seven),
            "8" => Some(Digit::Eight),
            "9" => Some(Digit::Nine),
            _ => None,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct Board(Vec<Vec<Option<Digit>>>);

impl Board {
    fn from_chars(chars: &[Vec<char>]) -> Self {
        let parsed_digits = chars
            .iter()
            .map(|row| row.iter().map(|c| Digit::parse(*c)).collect())
            .collect();

        Self(parsed_digits)
    }

    fn lookup(&self, row_index: usize, column_index: usize) -> Option<Digit> {
        match self.0.get(row_index).and_then(|row| row.get(column_index)) {
            Some(Some(digit)) => Some(*digit),
            _ => None,
        }
    }
}

fn is_section_valid(tiles: &[Option<Digit>]) -> bool {
    let all_digits = tiles.iter().filter_map(|d| *d).collect::<Vec<Digit>>();
    let unique_digits = BTreeSet::from_iter(&all_digits);
    all_digits.len() == unique_digits.len()
}

impl Solution {
    pub fn is_valid_sudoku(chars: Vec<Vec<char>>) -> bool {
        let board = Board::from_chars(&chars);

        for row in &board.0 {
            if !is_section_valid(row) {
                return false;
            }
        }

        for column_index in 0..10 {
            let column = (0..10)
                .map(|row_index| board.lookup(row_index, column_index))
                .collect::<Vec<Option<Digit>>>();

            if !is_section_valid(&column) {
                return false;
            }
        }

        for start_column in [0, 3, 6] {
            for start_row in [0, 3, 6] {
                let grid2: Vec<_> = (start_row..start_row + 3)
                    .flat_map(|row_index| {
                        (start_column..start_column + 3)
                            .map(|column_index| board.lookup(row_index, column_index))
                            .collect::<Vec<Option<Digit>>>()
                    })
                    .collect();

                if !is_section_valid(&grid2) {
                    return false;
                }
            }
        }

        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(expected: bool, board: Vec<Vec<char>>) {
        let actual = Solution::is_valid_sudoku(board);
        assert_eq!(actual, expected);
    }

    #[test]
    fn case_1() {
        run_case(
            true,
            vec![
                vec!['5', '3', '.', '.', '7', '.', '.', '.', '.'],
                vec!['6', '.', '.', '1', '9', '5', '.', '.', '.'],
                vec!['.', '9', '8', '.', '.', '.', '.', '6', '.'],
                vec!['8', '.', '.', '.', '6', '.', '.', '.', '3'],
                vec!['4', '.', '.', '8', '.', '3', '.', '.', '1'],
                vec!['7', '.', '.', '.', '2', '.', '.', '.', '6'],
                vec!['.', '6', '.', '.', '.', '.', '2', '8', '.'],
                vec!['.', '.', '.', '4', '1', '9', '.', '.', '5'],
                vec!['.', '.', '.', '.', '8', '.', '.', '7', '9'],
            ],
        );
    }

    #[test]
    fn case_2() {
        run_case(
            false,
            vec![
                vec!['8', '3', '.', '.', '7', '.', '.', '.', '.'],
                vec!['6', '.', '.', '1', '9', '5', '.', '.', '.'],
                vec!['.', '9', '8', '.', '.', '.', '.', '6', '.'],
                vec!['8', '.', '.', '.', '6', '.', '.', '.', '3'],
                vec!['4', '.', '.', '8', '.', '3', '.', '.', '1'],
                vec!['7', '.', '.', '.', '2', '.', '.', '.', '6'],
                vec!['.', '6', '.', '.', '.', '.', '2', '8', '.'],
                vec!['.', '.', '.', '4', '1', '9', '.', '.', '5'],
                vec!['.', '.', '.', '.', '8', '.', '.', '7', '9'],
            ],
        );
    }

    #[test]
    fn case_3() {
        run_case(
            false,
            vec![
                vec!['.', '.', '4', '.', '.', '.', '6', '3', '.'],
                vec!['.', '.', '.', '.', '.', '.', '.', '.', '.'],
                vec!['5', '.', '.', '.', '.', '.', '.', '9', '.'],
                vec!['.', '.', '.', '5', '6', '.', '.', '.', '.'],
                vec!['4', '.', '3', '.', '.', '.', '.', '.', '1'],
                vec!['.', '.', '.', '7', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '5', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '.', '.', '.', '.'],
            ],
        );
    }

    #[test]
    fn case_4() {
        run_case(
            false,
            vec![
                vec!['.', '.', '.', '.', '5', '.', '.', '1', '.'],
                vec!['.', '4', '.', '3', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '3', '.', '.', '1'],
                vec!['8', '.', '.', '.', '.', '.', '.', '2', '.'],
                vec!['.', '.', '2', '.', '7', '.', '.', '.', '.'],
                vec!['.', '1', '5', '.', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '2', '.', '.', '.'],
                vec!['.', '2', '.', '9', '.', '.', '.', '.', '.'],
                vec!['.', '.', '4', '.', '.', '.', '.', '.', '.'],
            ],
        );
    }
}
