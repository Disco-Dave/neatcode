/// https://leetcode.com/problems/valid-sudoku
pub struct Solution;

use std::collections::BTreeSet;
use std::iter::FromIterator;

fn all_rows() -> Vec<Vec<(usize, usize)>> {
    (0..9)
        .map(|row_index| {
            (0..9)
                .map(|column_index| (row_index, column_index))
                .collect()
        })
        .collect()
}

fn all_columns() -> Vec<Vec<(usize, usize)>> {
    (0..9)
        .map(|column_index| (0..9).map(|row_index| (row_index, column_index)).collect())
        .collect()
}

fn all_squares() -> Vec<Vec<(usize, usize)>> {
    let mut squares = vec![];

    let starts = [0, 3, 6];

    for start_column in starts {
        for start_row in starts {
            let square = (start_row..start_row + 3)
                .flat_map(|row_index| {
                    (start_column..start_column + 3)
                        .map(|column_index| (row_index, column_index))
                        .collect::<Vec<_>>()
                })
                .collect();

            squares.push(square)
        }
    }

    squares
}

fn is_valid_section(board: &[Vec<char>], section: &[(usize, usize)]) -> bool {
    let all_digits = section
        .iter()
        .filter_map(|(row_index, column_index)| {
            let tile = board[*row_index][*column_index];

            if tile == '.' {
                None
            } else {
                Some(tile)
            }
        })
        .collect::<Vec<_>>();

    let unique_digits = BTreeSet::from_iter(&all_digits);

    all_digits.len() == unique_digits.len()
}

impl Solution {
    pub fn is_valid_sudoku(board: Vec<Vec<char>>) -> bool {
        all_columns()
            .into_iter()
            .chain(all_rows())
            .chain(all_squares())
            .all(|section| is_valid_section(&board, &section))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(expected: bool, board: Vec<Vec<char>>) {
        let actual = Solution::is_valid_sudoku(board);
        assert_eq!(actual, expected);
    }

    #[test]
    fn case_1() {
        run_case(
            true,
            vec![
                vec!['5', '3', '.', '.', '7', '.', '.', '.', '.'],
                vec!['6', '.', '.', '1', '9', '5', '.', '.', '.'],
                vec!['.', '9', '8', '.', '.', '.', '.', '6', '.'],
                vec!['8', '.', '.', '.', '6', '.', '.', '.', '3'],
                vec!['4', '.', '.', '8', '.', '3', '.', '.', '1'],
                vec!['7', '.', '.', '.', '2', '.', '.', '.', '6'],
                vec!['.', '6', '.', '.', '.', '.', '2', '8', '.'],
                vec!['.', '.', '.', '4', '1', '9', '.', '.', '5'],
                vec!['.', '.', '.', '.', '8', '.', '.', '7', '9'],
            ],
        );
    }

    #[test]
    fn case_2() {
        run_case(
            false,
            vec![
                vec!['8', '3', '.', '.', '7', '.', '.', '.', '.'],
                vec!['6', '.', '.', '1', '9', '5', '.', '.', '.'],
                vec!['.', '9', '8', '.', '.', '.', '.', '6', '.'],
                vec!['8', '.', '.', '.', '6', '.', '.', '.', '3'],
                vec!['4', '.', '.', '8', '.', '3', '.', '.', '1'],
                vec!['7', '.', '.', '.', '2', '.', '.', '.', '6'],
                vec!['.', '6', '.', '.', '.', '.', '2', '8', '.'],
                vec!['.', '.', '.', '4', '1', '9', '.', '.', '5'],
                vec!['.', '.', '.', '.', '8', '.', '.', '7', '9'],
            ],
        );
    }

    #[test]
    fn case_3() {
        run_case(
            false,
            vec![
                vec!['.', '.', '4', '.', '.', '.', '6', '3', '.'],
                vec!['.', '.', '.', '.', '.', '.', '.', '.', '.'],
                vec!['5', '.', '.', '.', '.', '.', '.', '9', '.'],
                vec!['.', '.', '.', '5', '6', '.', '.', '.', '.'],
                vec!['4', '.', '3', '.', '.', '.', '.', '.', '1'],
                vec!['.', '.', '.', '7', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '5', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '.', '.', '.', '.'],
            ],
        );
    }

    #[test]
    fn case_4() {
        run_case(
            false,
            vec![
                vec!['.', '.', '.', '.', '5', '.', '.', '1', '.'],
                vec!['.', '4', '.', '3', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '3', '.', '.', '1'],
                vec!['8', '.', '.', '.', '.', '.', '.', '2', '.'],
                vec!['.', '.', '2', '.', '7', '.', '.', '.', '.'],
                vec!['.', '1', '5', '.', '.', '.', '.', '.', '.'],
                vec!['.', '.', '.', '.', '.', '2', '.', '.', '.'],
                vec!['.', '2', '.', '9', '.', '.', '.', '.', '.'],
                vec!['.', '.', '4', '.', '.', '.', '.', '.', '.'],
            ],
        );
    }
}
