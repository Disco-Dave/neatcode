/// https://leetcode.com/problems/top-k-frequent-elements/
pub struct Solution;

use std::collections::BTreeMap;
use std::convert::TryInto;

impl Solution {
    pub fn top_k_frequent(nums: Vec<i32>, k: i32) -> Vec<i32> {
        let mut running_frequencies = BTreeMap::<i32, u64>::new();

        for num in nums {
            let entry = running_frequencies.entry(num).or_insert(0);
            *entry += 1;
        }

        let mut total_frequencies = running_frequencies
            .into_iter()
            .map(|e| (e.1, e.0))
            .collect::<Vec<(u64, i32)>>();

        total_frequencies.sort_by(|a, b| (b.0).cmp(&a.0));

        total_frequencies
            .into_iter()
            .map(|(_, b)| b)
            .take(k.try_into().unwrap_or(0))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solves_first_example() {
        let nums = vec![1, 1, 1, 2, 2, 3];
        let k = 2;

        let expected = vec![1, 2];
        let actual = Solution::top_k_frequent(nums, k);

        assert_eq!(actual, expected);
    }

    #[test]
    fn solves_second_example() {
        let nums = vec![1];
        let k = 1;

        let expected = vec![1];
        let actual = Solution::top_k_frequent(nums, k);

        assert_eq!(actual, expected);
    }
}
