/// https://leetcode.com/problems/valid-anagram
pub struct Solution;

fn sort_chars(s: &str) -> Vec<char> {
    let mut characters = s.chars().collect::<Vec<char>>();
    characters.sort();
    characters
}

impl Solution {
    pub fn is_anagram(s: &str, t: &str) -> bool {
        sort_chars(s) == sort_chars(t)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(s: &str, t: &str, expected: bool) {
        let actual = Solution::is_anagram(s, t);
        assert_eq!(actual, expected);
    }

    #[test]
    fn case_1() {
        run_case("anagram", "nagaram", true);
    }

    #[test]
    fn case_2() {
        run_case("rat", "car", false);
    }
}
