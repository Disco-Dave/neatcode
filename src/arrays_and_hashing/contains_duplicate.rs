/// https://leetcode.com/problems/contains-duplicate/
pub struct Solution;

use std::collections::HashSet;

impl Solution {
    pub fn contains_duplicate(nums: &[i32]) -> bool {
        let mut seen_nums = HashSet::new();
        nums.iter().any(|num| !seen_nums.insert(*num))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(input: &[i32], expected: bool) {
        let actual = Solution::contains_duplicate(input);
        assert_eq!(actual, expected)
    }

    #[test]
    fn case_1() {
        let input = vec![1, 2, 3, 1];
        run_case(&input, true);
    }

    #[test]
    fn case_2() {
        let input = vec![1, 2, 3, 4];
        run_case(&input, false);
    }

    #[test]
    fn case_3() {
        let input = vec![1, 1, 1, 3, 3, 4, 3, 2, 4, 2];
        run_case(&input, true);
    }
}
