/// https://leetcode.com/problems/two-sum
pub struct Solution;

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        for (outer_index, outer_value) in nums.iter().enumerate() {
            for (inner_index, inner_value) in nums.iter().enumerate() {
                if outer_index != inner_index && (outer_value + inner_value) == target {
                    return vec![outer_index as i32, inner_index as i32];
                }
            }
        }

        vec![]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run_case(nums: Vec<i32>, target: i32, expected: Vec<i32>) {
        let actual = Solution::two_sum(nums, target);
        assert_eq!(actual, expected)
    }

    #[test]
    fn case_1() {
        let nums = vec![2, 7, 11, 15];
        let target = 9;
        let expected = vec![0, 1];
        run_case(nums, target, expected);
    }

    #[test]
    fn case_2() {
        let nums = vec![3, 2, 4];
        let target = 6;
        let expected = vec![1, 2];
        run_case(nums, target, expected);
    }

    #[test]
    fn case_3() {
        let nums = vec![3, 3];
        let target = 6;
        let expected = vec![0, 1];
        run_case(nums, target, expected);
    }
}
