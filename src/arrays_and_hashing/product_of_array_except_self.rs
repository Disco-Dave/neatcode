/// https://leetcode.com/problems/product-of-array-except-self/
pub struct Solution;

impl Solution {
    pub fn product_except_self(nums: Vec<i32>) -> Vec<i32> {
        (0..nums.len())
            .map(|index| {
                let left: i32 = nums[..index].iter().product();
                let right: i32 = nums[index + 1..].iter().product();

                left * right
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solves_first_example() {
        let nums = vec![1, 2, 3, 4];
        let expected = vec![24, 12, 8, 6];

        let actual = Solution::product_except_self(nums);

        assert_eq!(expected, actual);
    }

    #[test]
    fn solves_second_example() {
        let nums = vec![-1, 1, 0, -3, 3];
        let expected = vec![0, 0, 9, 0, 0];

        let actual = Solution::product_except_self(nums);

        assert_eq!(expected, actual);
    }
}
